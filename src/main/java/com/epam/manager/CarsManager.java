package com.epam.manager;

import com.epam.domain.Car;
import com.epam.repository.CarsRepository;
import java.time.Year;
import java.util.Arrays;

public class CarsManager {

  private final CarsRepository repository;
  private Car[] foundCars;

  public CarsManager(CarsRepository repository) {
    this.repository = repository;
  }

  public void add(Car car) {
    repository.save(car);
  }

  public Car[] findAllByBrand(String brand) {
    foundCars = new Car[0];
    for (Car car : repository.getAll()) {
      if (car.getBrand().equals(brand)) {
        foundCars = addToFoundCars(foundCars, car);
      }
    }
    return foundCars;
  }

  public Car[] findAllByModelAndYear(String model, int year) {
    foundCars = new Car[0];
    for (Car car : repository.getAll()) {
      int yearsOfUse = Year.now().getValue() - car.getReleaseYear();
      if (car.getModel().equals(model) && yearsOfUse > year) {
        foundCars = addToFoundCars(foundCars, car);
      }
    }
    return foundCars;
  }

  public Car[] findAllByYearAndPriceOver(int year, int price) {
    foundCars = new Car[0];
    for (Car car : repository.getAll()) {
      if (car.getReleaseYear() == year && car.getPrice() > price) {
        foundCars = addToFoundCars(foundCars, car);
      }
    }
    return foundCars;
  }

  private Car[] addToFoundCars(Car[] array, Car car) {
    Car[] tmp = new Car[array.length + 1];
    System.arraycopy(array, 0, tmp, 0, array.length);
    tmp[tmp.length - 1] = car;
    return tmp;
  }

  @Override
  public String toString() {
    return "CarRepository{" +
        "cars=" + Arrays.toString(repository.getAll()) +
        '}';
  }
}
