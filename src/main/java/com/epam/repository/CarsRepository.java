package com.epam.repository;

import com.epam.domain.Car;

public class CarsRepository {
 private Car[] cars = new Car[0];

  public void save(Car car) {
    Car[] tmp = new Car[cars.length + 1];
    System.arraycopy(cars, 0, tmp, 0, cars.length);
    tmp[tmp.length - 1] = car;
    cars = tmp;
  }

  public Car[] getAll() {
    return cars;
  }
}
