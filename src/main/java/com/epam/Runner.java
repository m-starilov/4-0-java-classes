package com.epam;

import com.epam.domain.Car;
import com.epam.manager.CarsManager;
import com.epam.repository.CarsRepository;
import java.util.Arrays;

public class Runner {

  public static void main(String[] args) {
    CarsManager carManager = new CarsManager(new CarsRepository());
    carManager.add(new Car(1, "LADA", "Vesta", 2019, "White", 10000, "133 abs 17"));
    carManager.add(new Car(11, "LADA", "Vesta", 2015, "Orange", 10000, "134 abs 17"));
    carManager.add(new Car(12, "LADA", "Vesta", 2016, "Gray", 10000, "135 abs 17"));
    carManager.add(new Car(2, "BMW", "320i", 2001, "Silver", 5000, "091 kxa 17"));
    carManager.add(new Car(3, "BMW", "M5", 2001, "Silver", 55000, "039 mre 01"));
    carManager.add(new Car(4, "Lotus", "Elise", 1999, "British green", 25000, "225 ter 02"));
    carManager.add(new Car(5, "Nissan", "Patrol", 2001, "Black", 25000, "854 qal 01"));
    carManager.add(new Car(6, "УАЗ", "Patriot", 2014, "Navy", 14000, "533 tgg 01"));
    System.out.println("Массив авто: " + carManager.toString());
    System.out.println("Список автомобилей BMW: " +
        Arrays.toString(carManager.findAllByBrand("BMW")));
    System.out.println("Список автомобилей Vesta со сроком эксплуатации более 5 лет: " +
        Arrays.toString(carManager.findAllByModelAndYear("Vesta", 5)));
    System.out.println("Список автомобилей 2001 года выпуска и ценой более 5000: " +
        Arrays.toString(carManager.findAllByYearAndPriceOver(2001, 5000)));
  }
}
