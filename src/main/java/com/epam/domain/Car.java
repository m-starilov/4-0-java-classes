package com.epam.domain;

import java.util.Objects;

public class Car {
  private int id;
  private String brand;
  private String model;
  private int releaseYear;
  private String color;
  private int price;
  private String licencePlate;

  public Car(int id, String brand, String model, int releaseYear, String color, int price,
      String licencePlate) {
    this.id = id;
    this.brand = brand;
    this.model = model;
    this.releaseYear = releaseYear;
    this.color = color;
    this.price = price;
    this.licencePlate = licencePlate;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public int getReleaseYear() {
    return releaseYear;
  }

  public void setReleaseYear(int releaseYear) {
    this.releaseYear = releaseYear;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getLicencePlate() {
    return licencePlate;
  }

  public void setLicencePlate(String licencePlate) {
    this.licencePlate = licencePlate;
  }

  @Override
  public String toString() {
    return "\n Car{" +
        "id=" + id +
        ", brand='" + brand + '\'' +
        ", model='" + model + '\'' +
        ", releaseYear=" + releaseYear +
        ", color='" + color + '\'' +
        ", price=" + price +
        ", licencePlate='" + licencePlate + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Car)) {
      return false;
    }

    Car car = (Car) o;

    if (id != car.id) {
      return false;
    }
    if (releaseYear != car.releaseYear) {
      return false;
    }
    if (price != car.price) {
      return false;
    }
    if (!Objects.equals(brand, car.brand)) {
      return false;
    }
    if (!Objects.equals(model, car.model)) {
      return false;
    }
    if (!Objects.equals(color, car.color)) {
      return false;
    }
    return Objects.equals(licencePlate, car.licencePlate);
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (brand != null ? brand.hashCode() : 0);
    result = 31 * result + (model != null ? model.hashCode() : 0);
    result = 31 * result + releaseYear;
    result = 31 * result + (color != null ? color.hashCode() : 0);
    result = 31 * result + price;
    result = 31 * result + (licencePlate != null ? licencePlate.hashCode() : 0);
    return result;
  }
}
